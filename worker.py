import configparser
import ast
import datetime
import socket
import struct
import re
import csv
import ipaddress

# read config
config = configparser.ConfigParser()
config.read('sample-config.ini')

rules = "# Beggining of WPWAF rules\n# GENERATED @ " + datetime.datetime.now().strftime("%d/%m/%Y  %H:%M:%S") + "\n\n\n\n"


print("# Working...\n\n")


# Define block status code
BLOCK_STATUS_CODE = int(config["GENERAL"]["BLOCK_STATUS_CODE"])

if BLOCK_STATUS_CODE not in range(200, 600):
	BLOCK_STATUS_CODE = 403

rules = rules + "# Define HTTP code response in case request is blocked\nset $BLOCK_STATUS_CODE "+ str(BLOCK_STATUS_CODE) + ";\n\n"


# Add WAF signature header
SIGNATURE_HEADER = config["GENERAL"]["ADD_SIGNATURE_HEADER"]

if SIGNATURE_HEADER == "1":
	rules = rules + "# Set WPWAF signature header\nadd_header X-WPWAF	present;\n\n"



# Block entire country IP block
countries = ast.literal_eval(config.get("GENERAL", "BLOCK_COUNTRY"))
ipdbfile = open("IP2LOCATION-LITE-DB1.CSV", "r", newline="")

# convert decimal to IP from CSV
def dec2ip(dec):
	return(str(socket.inet_ntoa(struct.pack('!L', int(float(dec))))))

if countries:
	rules = rules + "# Blocking countries\n"
	reader = csv.reader(ipdbfile, delimiter=",")
	for row in reader:
		if str(row[2]) in countries:
			for block in ipaddress.summarize_address_range( ipaddress.IPv4Address(dec2ip(row[0])),  ipaddress.IPv4Address(dec2ip(row[1]))):
				rules = rules + "deny " + str(block) + "; # Blocking "+str(row[2])+" IP block\n"

rules = rules + "allow all;\n\n"



# Block HTTP methods
blocked_methods = ast.literal_eval(config.get("GENERAL", "BLOCK_METHOD"))

if blocked_methods:
	rules = rules + "# Block unwanted HTTP methods;\n"
	for method in blocked_methods:
		rules = rules + 'if ($request_method = "'+method+'") { return '+ str(BLOCK_STATUS_CODE)+'; } \n'
	rules = rules + "\n\n"

# Block user agents

blocked_ua = ast.literal_eval(config.get("GENERAL", "BLOCK_UA"))
if blocked_ua:
	uamap = ""
	rules = rules + "# Block unwanted HTTP methods;\nmap $http_user_agent $is_bot {\ndefault 0; \n"
	for ua in blocked_ua:
		uamap = uamap + "'~*"+ua+"' 1;\n"
	rules = rules + uamap +"}\n\n"
	rules = rules + 'if ($is_bot = 1) { return '+ str(BLOCK_STATUS_CODE)+'; } \n\n'



print(rules)